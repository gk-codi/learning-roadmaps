import app from './app'
import initializeDatabase from './db'


const start = async () => {
  const controller = await initializeDatabase();

  app.get("/", (req, res, next) => res.send("ok"));


  // **************** user profile controlers ********************
  // Create user profile

  app.post("/users/new", async (req, res, next) => {
    try {
      const { user_name, user_career, user_email, user_bio, user_image, user_facebook, user_twitter, user_linkedin } = req.query;
      const result = await controller.createuserprofile({ user_name, user_email, user_career, user_bio, user_image, user_facebook, user_twitter, user_linkedin });
      res.json({ success: true, result });
    } catch (e) {
      next(e);
    }
  });

  // View user profile

  app.get("/users/view/:id", async (req, res, next) => {
    try {
      const { id } = req.params;
      const user = await controller.viewuserprofile(id);
      res.json({ success: true, result: user });
    } catch (e) {
      next(e);
    }
  });

  // Update user profile

  app.post("/users/update/:id", async (req, res, next) => {
    try {
      const { id } = req.params;
      const { user_name, user_career, user_email, user_bio, user_image, user_facebook, user_twitter, user_linkedin } = req.query;
      const result = await controller.updateuserprofile(id, { user_name, user_email, user_career, user_bio, user_image, user_facebook, user_twitter, user_linkedin });
      res.json({ success: true, result });
    } catch (e) {
      next(e);
    }
  });

  // ************************ Map controlers ****************************
  // Create new map

  app.post("/maps/new", async (req, res, next) => {
    try {
      const { map_title, map_description, map_public, map_image, user_profile_user_id, map_fork_id, map_tags } = req.query;
      const result = await controller.createnewmap({ map_title, map_description, map_public, map_image, user_profile_user_id, map_fork_id, map_tags });
      res.json({ success: true, result });
    } catch (e) {
      next(e);
    }
  });

// Update map

app.post("/maps/update/:id", async (req, res, next) => {
  try {
    const { id } = req.params;
    const { map_title, map_description, map_public, map_image, user_profile_user_id, map_fork_id, map_tags } = req.query;
    const result = await controller.updatelearnmap(id,{ map_title, map_description, map_public, map_image, user_profile_user_id, map_fork_id, map_tags });
    res.json({ success: true, result });
  } catch (e) {
    next(e);
  }
});

// View map

app.get("/maps/view/:id", async (req, res, next) => {
  try {
    const { id } = req.params;
    const user = await controller.viewmap(id);
    res.json({ success: true, result: user });
  } catch (e) {
    next(e);
  }
});

// Fork map

app.post("/maps/fork/:id", async (req, res, next) => {
  try {
    const { id } = req.params;
    const map_id = id
    const {user_id} = req.query;
    const result = await controller.forkmap({ map_id, user_id  });
    res.json({ success: true, result });
  } catch (e) {
    next(e);
  }
});


// List of maps

app.get("/maps/list", async (req, res, next) => {
  try {
    const { order } = req.query;
    const maps = await controller.getmaplist(order);
    res.json({ success: true, result: maps });
  } catch (e) {
    next(e);
  }
});


// Delete map

app.get("/maps/delete/:id", async (req, res, next) => {
  try {
    const { id } = req.params;
    const result = await controller.deletelearnmap(id);
    res.json({ success: true, result });
  } catch (e) {
    next(e);
  }
});


// ******************** Map items **************************

// create new item

app.post("/mapitem/new", async (req, res, next) => {
  try {
    const { item_title, item_description, item_checked, item_image, learn_map_map_id, user_profile_user_id } = req.query;
    const result = await controller.createnewmapitem({ item_title, item_description, item_checked, item_image, learn_map_map_id, user_profile_user_id });
    res.json({ success: true, result });
  } catch (e) {
    next(e);
  }
});



// view map item (It'sshowing all items)

app.get("/mapitem/view/:id", async (req, res, next) => {
  try {
    const { id } = req.params;
    const user = await controller.viewmapitem(id);
    res.json({ success: true, result: user });
  } catch (e) {
    next(e);
  }
});

// List of map items

app.get("/mapitem/list", async (req, res, next) => {
  try {
    const { order } = req.query;
    const maps = await controller.listmapitems(order);
    res.json({ success: true, result: maps });
  } catch (e) {
    next(e);
  }
});




// Delete map item

app.get("/mapitem/delete/:id", async (req, res, next) => {
  try {
    const { id } = req.params;
    const result = await controller.deletemapitem(id);
    res.json({ success: true, result });
  } catch (e) {
    next(e);
  }
});

// update map item


app.post("/mapitem/update/:id", async (req, res, next) => {
  try {
    const { id } = req.params;
    const { item_title, item_description, item_checked, item_image, learn_map_map_id } = req.query;
    const result = await controller.updatemapitem(id,{ item_title, item_description, item_checked, item_image, learn_map_map_id });
    res.json({ success: true, result });
  } catch (e) {
    next(e);
  }
});



app.get("/mapitem/check/:id", async (req, res, next) => {
  try {
    const { id } = req.params;
    const { item_checked } = req.query;
    const result = await controller.checkmapitem(id,{ item_checked });
    res.json({ success: true, result });
  } catch (e) {
    next(e);
  }
});















  // ERROR
  app.use((err, req, res, next) => {
    console.error(err)
    const message = err.message
    res.status(500).json({ success:false, message })
  })
  
  app.listen(8080, () => console.log("server listening on port 8080"));
};

start();

  //app.get('/',(req,res)=>res.send("ok"));
  //app.listen(8080, ()=>console.log('server listening on port 8080'))



