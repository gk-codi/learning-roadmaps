import React, { Component } from 'react';
import { makeRequestUrl } from "../uutils.js";






const makeUrl = (path, params) =>
  makeRequestUrl(`http://localhost:8080/${path}`, params);



class userprofile extends Component {

  state={
    users_list:[],
    error_massage:"",
    user_bio: "",
    user_name: "",
    user_email: "",
    user_image: "",
    user_facebook: "",
    user_linkedin: "",
    user_career: "",
    isLoading:false

  }

  createuserprofile = async props => {
    try {
      if(!props || !props.user_name || !props.user_email || !props.user_bio) {
        throw new Error('you must provide a name and an email and bio and');

      }
      const {user_name, user_email, user_bio, user_image, user_career, user_facebook, user_linkedin, user_twitter} = props;
      const response = await fetch(
        `http://localhost:8080/users/new/?name=${user_name}&email=${user_email}&bio=${user_bio}&career=${user_career}&user_image=${user_image}&user_linkedin=${user_linkedin}&user_twitter=${user_twitter}&user_facebook=${user_facebook}`
      );
      const answer = await response.json();
      if (answer.success){
        const id = answer.result;
        const user = {user_name, user_email, user_bio, user_image, user_career, user_facebook, user_linkedin, user_twitter, id};
        const users_list = [...this.state.members_list, user]
        this.setState({ users_list });
      } else {
        this.setState({error_message: answer.message })
}
      
      }      catch (err) {
        this.setState({ error_message: err.message });
    
  }
};

viewpuserrofile = async id => {
  const previous_user = this.state.users_list.find(
    user => user.id === id
  );
  if (previous_user) {
    return
  }
  try {
    const response = await fetch(`http://localhost:8080/users/view/${id}`);
    const answer = await response.json();
    if (answer) {
      const user = answer.result;
      const users_list = [...this.state.members_list, user];
      this.setState({ users_list });
        
    }
    else {
      this.setState({ error_message: answer.message });
    }
} catch (err) {
  this.setState({ error_message: err.message });

}
};

updateuserprofile = async (id, props) => {
  try {
    if (!props || !(props.user_name || props.user_email || props.user_bio )) {
      throw new Error(
        `you need at least name or email and bio properties to update a member`
      );
    }
  const url = makeUrl(`users/update/${id}`, {
    user_name: props.user_name,
    user_bio: props.user_bio,
    user_career: props.user_career,
    user_image: props.user_image,
    user_facebook: props.user_facebook,
    user_linkedin: props.user_linkedin
  });

  const response = await fetch(url, {
    method:'POST', 
     });
  const answer = await response.json();
  if (answer.success) {
    // we update the user, to reproduce the database changes:
    const users_list = this.state.members_list.map(user => {
      // if this is the contact we need to change, update it. This will apply to exactly
      // one contact
      if (user.id === id) {
        const new_user = {
          id: user.id,
          name: props.user_name || user.user_name,
          image: props.user_image || user.user_image,
          bio: props.user_bio || user.user_bio,
          career: props.user_career || user.user_career,
          facebook: props.user_facebook || user.user_facebook,
          twitter: props.user_twitter || user.user_twitter
          };
          
        return new_user;
      }
      // otherwise, don't change the contact at all
      else {
        return user;
      }
    });
    this.setState({ users_list });
  } else {
    this.setState({ error_message: answer.message });
  }
} catch (err) {
  this.setState({ error_message: err.message });
}
};  


componentDidMount() {
  this.getmemberslist();
}
onSubmit = (evt) => {
  // stop the form from submitting:
  evt.preventDefault();
  const image = evt.target.fileField.files[0]

  // extract name and position and image from state
  const { name, position } = this.state;
  // create the member form name and position and image
  console.log(name, position, image)
  this.createmember({ name, position, image });
  // empty name and position and image so the text input fields are reset
  this.setState({ name: "", position: "", image: "" });
};

 

  render() {
    return (
      <div>
        
      </div>
    );
  }
}

export default userprofile;